import { Component,OnInit,AfterContentInit } from '@angular/core';


import * as  $ from 'jquery';
import { Router } from '@angular/router';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Spa&Alternativa';


  constructor( private router: Router) {

  }

  ngAfterContentInit(): void {

    let ruta;
    this.router.events.subscribe((function (event) {

        ruta = window.location.pathname;
        console.log(ruta);
        if (ruta == "/contacto" || ruta=="/agendar") {
            ocultarContenido();
            
        } else {
            mostrarContenido();
        }

    }));


    function mostrarContenido() {
        $('.home').show();
      
    }

    function ocultarContenido() {

        $('.home').hide();
     
        // $('#footer').show();
    }
  }
}

