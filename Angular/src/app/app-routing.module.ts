import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
//importacion de componentes
import {ContactoComponent} from './componentes/contacto/contacto.component';
import {AgendarComponent} from './componentes/agendar/agendar.component';

const routes: Routes = [
  //rutas ligadas a componentes
  {path:'contacto' , component:ContactoComponent},
  {path:'agendar' , component:AgendarComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
//exportacion de componentes
export class AppRoutingModule {ContactoComponent; AgendarComponent }
