import { Component, OnInit } from '@angular/core';
import * as  $ from 'jquery';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  flag = false;

  constructor() { }


  ngOnInit(): void {

  }

  toggle = function () {

    if (screen.width < 900) {
      if (this.flag == false) {
        $('nav ul').show();
        this.flag = true;
      }
      else{
        $('nav ul').hide();
        this.flag = false;
      }
    }
  
  }


  ngAfterContentInit(): void {

    $(document).ready(function () {

      if (screen.width < 900) {
        $('nav ul').hide()
      } else if (screen.width > 900) {
        $('nav ul').show()
      }

      $(window).resize(function () {

        if (screen.width < 900 && screen.width > 870) {
          $('nav ul').hide()
        } else if (screen.width > 900) {
          $('nav ul').show()
        }


      });

    });

  }
}
