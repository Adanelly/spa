import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
/*import { AgmCoreModule } from '@agm/core';*/

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ServiciosComponent } from './servicios/servicios.component';
import { InicioComponent } from './componentes/inicio/inicio.component';
import { PruebasComponent } from './componentes/pruebas/pruebas.component';
import { MenuComponent } from './componentes/menu/menu.component';
import { FooterComponent } from './componentes/footer/footer.component';
import { ContactoComponent } from './componentes/contacto/contacto.component';
import { AgendarComponent } from './componentes/agendar/agendar.component';


@NgModule({
  declarations: [
    AppComponent,
    ServiciosComponent,
    InicioComponent,
    PruebasComponent,
    ServiciosComponent,
    MenuComponent,
    FooterComponent,
    ContactoComponent,
    AgendarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
   /* AgmCoreModule.forRoot({
      apiKey: ''
      })*/
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
